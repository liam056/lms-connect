var http = require('http');
var LMSConnect = require('lmsconnect');
var express = require('express')
var swig = require('swig');
var config = require("./config");
var Api = require("./api");

var lms = new LMSConnect(config)
var app = express();
var api = new Api(lms);

lms.connect(function(ready){
    
    if(ready)
    {
        app.engine('html', swig.renderFile);
        app.set('view engine', 'html');
        app.set('views', __dirname + '/views');

        app.use('/css', express.static(__dirname + '/views/css'));
        app.use('/js', express.static(__dirname + '/views/js'));
        app.use('/bootstrap-3.3.4-dist', express.static(__dirname + '/views/bootstrap-3.3.4-dist'));

        app.get('/', function(req, res) {
            res.render('index');
        })
        app.use('/api', api.router);
        app.listen(80);
    }
    else
    {
        console.log("Error.");
        process.exit(1);
    }
});
