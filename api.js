var express = require('express');
var lms = undefined;

function Api( lmsystem )
{
    lms = lmsystem;
    this.router = express.Router();
    
    // middleware specific to this router
    this.router.use(function timeLog(req, res, next) {
      console.log('Time: ', Date.now());
      next();
    })
    // define the home page route
    this.router.get('/', function(req, res) {
      res.json({"data":lms.getAll(),"map":lms.getMap(),"name":lms.getLMSName()});
    })
    // define the channel route
    this.router.route('/input/:id')
        .get(function(req, res) {
            res.json(lms.getInput(parseInt(req.params.id)));
        });
    
    this.router.route('/input/:id/gain/:gain')
        .get(function(req, res) {
            var input = lms.getInput(parseInt(req.params.id));
            var response = { "success" : input.setGain(parseInt(req.params.gain)) };
            lms.updateInput(input, parseInt(req.params.id));
            res.json(response);
        });
    
    this.router.route('/input/:id/mute/:mute')
        .get(function(req, res) {
            var input = lms.getInput(parseInt(req.params.id));
            var response = { "success" : input.setMute(parseInt(req.params.mute)) };
            lms.updateInput(input, parseInt(req.params.id));
            res.json(response);
        });
    
    this.router.route('/input/:id/save/:name')
        .get(function(req, res) {
            var input = lms.getInput(parseInt(req.params.id));
            lms.storeInput(input, req.params.name, function(done){
                res.json({success:done});
            });
        });
    
    this.router.route('/input/search/:name')
        .get(function(req, res) {
            lms.searchInput(req.params.name, function(done){
                res.json({success:done});
            });
        });

    this.router.route('/input')
        .get(function(req, res) {
            lms.searchAllInput(function(done){
                res.json({success:done});
            });
        });
    
    this.router.route('/output/:id')
        .get(function(req, res) {    
            res.json(lms.getOutput(parseInt(req.params.id)));
        });
    
    this.router.route('/output/:id/gain/:gain')
        .get(function(req, res) {
            var output = lms.getOutput(parseInt(req.params.id));
            var response = { "success" : output.setGain(parseInt(req.params.gain)) };
            lms.updateOutput(output, parseInt(req.params.id));
            res.json(response);
        });
    
    this.router.route('/output/:id/mute/:mute')
        .get(function(req, res) {
            var output = lms.getOutput(parseInt(req.params.id));
            var response = { "success" : output.setMute(parseInt(req.params.mute)) };
            lms.updateOutput(output, parseInt(req.params.id));
            res.json(response);
        });
    
    this.router.route('/output/:id/highpass/type/:type')
        .get(function(req, res) {
            var output = lms.getOutput(parseInt(req.params.id));
            var response = { "success" : output.setHighpassType(req.params.type) };
            lms.updateOutput(output, parseInt(req.params.id));
            res.json(response);
        });
    
    this.router.route('/output/:id/lowpass/type/:type')
        .get(function(req, res) {
            var output = lms.getOutput(parseInt(req.params.id));
            var response = { "success" : output.setLowpassType(req.params.type) };
            lms.updateOutput(output, parseInt(req.params.id));
            res.json(response);
        });
    
    this.router.route('/output/:id/highpass/frequency/:frequency')
        .get(function(req, res) {
            var output = lms.getOutput(parseInt(req.params.id));
            var response = { "success" : output.setHighpassFrequency(req.params.frequency) };
            lms.updateOutput(output, parseInt(req.params.id));
            res.json(response);
        });
    
    this.router.route('/output/:id/lowpass/frequency/:frequency')
        .get(function(req, res) {
            var output = lms.getOutput(parseInt(req.params.id));
            var response = { "success" : output.setLowpassFrequency(req.params.frequency) };
            lms.updateOutput(output, parseInt(req.params.id));
            res.json(response);
        });
    
    this.router.route('/output/:id/save/:name')
        .get(function(req, res) {
            var input = lms.getOutput(parseInt(req.params.id));
            lms.storeOutput(input, req.params.name, function(done){
                res.json({success:done});
            });
        });
    
    this.router.route('/output/search/:name')
        .get(function(req, res) {
            lms.searchOutput(req.params.name, function(done){
                res.json({success:done});
            });
        });
    this.router.route('/output')
        .get(function(req, res) {
            lms.searchAllOutput(function(done){
                res.json({success:done});
            });
        });
    
    this.router.route('/map')
        .get(function(req, res) {    
            res.json(lms.getMap());
        });
    
    
    // define the about route
    this.router.get('/about', function(req, res) {
      res.json(JSON.stringify(lms.about()));
    })
};

Api.prototype.getRouter = function()
{
    console.dir("Router: "+this.router);
    return this.router;
}

module.exports = Api;