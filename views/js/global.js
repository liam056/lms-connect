var channels = {};
var map = {};
var name = "";
var channel = 0
var type = "";
var returnData = [];

$( "#connect" ).on( "click", function() {
    update(function(con){
        connected(con);
    });
});

var updateChannel = {
    input : function(data) {
        console.log("test");
        console.dir(data);
        setInputMute(data.mute, channel);
        $('#input'+channel+' .gain').val(data.gain);
        console.log(data.gain);
        $('#input'+channel+' span.gain').html(gainToString(calculateGain(data.gain)));
    },
    output : function(data) {
        setOutputMute(data.mute, channel);
        $('#output'+channel+' .gain').val(data.gain);
        $('#output'+channel+' span.gain').html(gainToString(calculateGain(data.gain)));
        setFrequency(data.lowpass.frequency, channel, 'lowpass');
        setFrequency(data.highpass.frequency, channel, 'highpass');
        setType(data.lowpass.type, channel, 'lowpass');
        setType(data.highpass.type, channel, 'highpass');
    }
};

$('#loadButton').on("click", function() {
    $('#loadModal').modal('hide')
    var data = returnData[$('#loadSelect').val()];
    delete data["name"];
    delete data["_id"];
    delete data["__v"];
    
    channels[type][channel] = data;
    
    updateChannel[type](channels[type][channel]);
    
    for( var i in data )
    {
        if ( typeof data[i] != "object" )
        {
            $.getJSON( "/api/"+type+"/"+channel+"/"+i+"/"+data[i] , function( data ) {
                if( data.error == "NOCONNECT" )
                {
                    console.log( "there was an error" );
                    connected(false);
                    $('#connectError').modal();
                }
            })
            .fail(function() {
                console.log( "Could not connect" );
                connected(false);
                $('#connectError').modal();
            })
        }
        else
        {
            for( var d in data[i] )
            {
                $.getJSON( "/api/"+type+"/"+channel+"/"+i+"/"+d+"/"+data[i][d] , function( data ) {
                    if( data.error == "NOCONNECT" )
                    {
                        console.log( "there was an error" );
                        connected(false);
                        $('#connectError').modal();
                    }
                })
                .fail(function() {
                    console.log( "Could not connect" );
                    connected(false);
                    $('#connectError').modal();
                })
            }
        }
    }
    
    
});

$('button.load').on( "click", function(){
    var id = $(this).parents('.channel').attr('id');
    channel = id.substring(id.length - 1);
    type = id.substring(0, id.length - 1);
    $.getJSON( "/api/"+type , function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
        else
        {
            $('#loadSelect').empty();

            for( var i in data.success )
            {
                returnData[i] = data.success[i];
                $('#loadSelect').append($("<option></option>").attr("value", i).text(data.success[i].name));
            }
            
            $('#loadModal').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('button.save').on( "click", function(){
    var id = $(this).parents('.channel').attr('id');
    channel = id.substring(id.length - 1);
    type = id.substring(0, id.length - 1);
    if ( $('#'+type+channel+' input[name="name"]').val().length == 0 )
    {
        $('#lengthError').modal();
    }
    else
    {
        $.getJSON( "/api/"+type+"/"+channel+"/save/"+$('#'+type+channel+' input[name="name"]').val(), function( data ) {
            if( data.error == "NOCONNECT" )
            {
                console.log( "there was an error" );
                connected(false);
                $('#connectError').modal();
            }
            else
            {
                $('#savedModal').modal();
            }
        })
        .fail(function() {
            console.log( "Could not connect" );
            connected(false);
            $('#connectError').modal();
        })
    }
});

$('#output0 .highpass select.type').change( function() {
    channels.output[0].highpass.type = $(this).val();
    console.log($(this).val());
    setType($(this).val(), 0, 'highpass');
    $.getJSON( "/api/output/0/highpass/type/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output0 .lowpass select.type').change( function() {
    channels.output[0].lowpass.type = $(this).val();
    setType($(this).val(), 0, 'lowpass');
    $.getJSON( "/api/output/0/lowpass/type/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output1 .highpass select.type').change( function() {
    channels.output[1].highpass.type = $(this).val();
    setType($(this).val(), 1, 'highpass');
    $.getJSON( "/api/output/1/highpass/type/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output1 .lowpass select.type').change( function() {
    channels.output[1].lowpass.type = $(this).val();
    setType($(this).val(), 1, 'lowpass');
    $.getJSON( "/api/output/1/lowpass/type/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output2 .highpass select.type').change( function() {
    channels.output[2].highpass.type = $(this).val();
    setType($(this).val(), 2, 'highpass');
    $.getJSON( "/api/output/2/highpass/type/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output2 .lowpass select.type').change( function() {
    channels.output[2].lowpass.type = $(this).val();
    setType($(this).val(), 2, 'lowpass');
    $.getJSON( "/api/output/2/lowpass/type/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output3 .highpass select.type').change( function() {
    channels.output[3].highpass.type = $(this).val();
    setType($(this).val(), 3, 'highpass');
    $.getJSON( "/api/output/3/highpass/type/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output3 .lowpass select.type').change( function() {
    channels.output[3].lowpass.type = $(this).val();
    setType($(this).val(), 3, 'lowpass');
    $.getJSON( "/api/output/3/lowpass/type/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output4 .highpass select.type').change( function() {
    channels.output[4].highpass.type = $(this).val();
    setType($(this).val(), 4, 'highpass');
    $.getJSON( "/api/output/4/highpass/type/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output4 .lowpass select.type').change( function() {
    channels.output[4].lowpass.type = $(this).val();
    setType($(this).val(), 4, 'lowpass');
    $.getJSON( "/api/output/4/lowpass/type/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output5 .highpass select.type').change( function() {
    channels.output[5].highpass.type = $(this).val();
    setType($(this).val(), 5, 'highpass');
    $.getJSON( "/api/output/5/highpass/type/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output5 .lowpass select.type').change( function() {
    channels.output[5].lowpass.type = $(this).val();
    setType($(this).val(), 5, 'lowpass');
    $.getJSON( "/api/output/5/lowpass/type/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output0 .highpass input.frequency').on( "change mousemove", function() {
    $('#output0 .highpass label.frequency').html(frequencyToString(map.frequency[$(this).val()]));
});
$('#output0 .highpass input.frequency').on( "change", function() {
    channels.output[0].highpass.frequency = $(this).val();
    $.getJSON( "/api/output/0/highpass/frequency/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});
$('#output0 .lowpass input.frequency').on( "change mousemove", function() {
    $('#output0 .lowpass label.frequency').html(frequencyToString(map.frequency[$(this).val()]));
});
$('#output0 .lowpass input.frequency').on( "change", function() {
    channels.output[0].lowpass.frequency = $(this).val();
    $.getJSON( "/api/output/0/lowpass/frequency/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output1 .highpass input.frequency').on( "change mousemove", function() {
    $('#output1 .highpass label.frequency').html(frequencyToString(map.frequency[$(this).val()]));
});
$('#output1 .highpass input.frequency').on( "change", function() {
    channels.output[1].highpass.frequency = $(this).val();
    $.getJSON( "/api/output/1/highpass/frequency/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});
$('#output1 .lowpass input.frequency').on( "change mousemove", function() {
    $('#output1 .lowpass label.frequency').html(frequencyToString(map.frequency[$(this).val()]));
});
$('#output1 .lowpass input.frequency').on( "change", function() {
    channels.output[1].lowpass.frequency = $(this).val();
    $.getJSON( "/api/output/1/lowpass/frequency/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output2 .highpass input.frequency').on( "change mousemove", function() {
    $('#output2 .highpass label.frequency').html(frequencyToString(map.frequency[$(this).val()]));
});
$('#output2 .highpass input.frequency').on( "change", function() {
    channels.output[2].highpass.frequency = $(this).val();
    $.getJSON( "/api/output/2/highpass/frequency/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});
$('#output2 .lowpass input.frequency').on( "change mousemove", function() {
    $('#output2 .lowpass label.frequency').html(frequencyToString(map.frequency[$(this).val()]));
});
$('#output2 .lowpass input.frequency').on( "change", function() {
    channels.output[2].lowpass.frequency = $(this).val();
    $.getJSON( "/api/output/2/lowpass/frequency/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output3 .highpass input.frequency').on( "change mousemove", function() {
    $('#output3 .highpass label.frequency').html(frequencyToString(map.frequency[$(this).val()]));
});
$('#output3 .highpass input.frequency').on( "change", function() {
    channels.output[3].highpass.frequency = $(this).val();
    $.getJSON( "/api/output/3/highpass/frequency/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});
$('#output3 .lowpass input.frequency').on( "change mousemove", function() {
    $('#output3 .lowpass label.frequency').html(frequencyToString(map.frequency[$(this).val()]));
});
$('#output3 .lowpass input.frequency').on( "change", function() {
    channels.output[3].lowpass.frequency = $(this).val();
    $.getJSON( "/api/output/3/lowpass/frequency/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output4 .highpass input.frequency').on( "change mousemove", function() {
    $('#output4 .highpass label.frequency').html(frequencyToString(map.frequency[$(this).val()]));
});
$('#output4 .highpass input.frequency').on( "change", function() {
    channels.output[4].highpass.frequency = $(this).val();
    $.getJSON( "/api/output/4/highpass/frequency/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});
$('#output4 .lowpass input.frequency').on( "change mousemove", function() {
    $('#output4 .lowpass label.frequency').html(frequencyToString(map.frequency[$(this).val()]));
});
$('#output4 .lowpass input.frequency').on( "change", function() {
    channels.output[4].lowpass.frequency = $(this).val();
    $.getJSON( "/api/output/4/lowpass/frequency/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output5 .highpass input.frequency').on( "change mousemove", function() {
    $('#output5 .highpass label.frequency').html(frequencyToString(map.frequency[$(this).val()]));
});
$('#output5 .highpass input.frequency').on( "change", function() {
    channels.output[5].highpass.frequency = $(this).val();
    $.getJSON( "/api/output/5/highpass/frequency/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});
$('#output5 .lowpass input.frequency').on( "change mousemove", function() {
    $('#output5 .lowpass label.frequency').html(frequencyToString(map.frequency[$(this).val()]));
});
$('#output5 .lowpass input.frequency').on( "change", function() {
    channels.output[5].lowpass.frequency = $(this).val();
    $.getJSON( "/api/output/5/lowpass/frequency/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#input0 input.gain').on( "change mousemove", function() {
    $('#input0 span.gain').html(gainToString(calculateGain($(this).val())));
});
$('#input0 input.gain').on( "change", function() {
    channels.input[0].gain = calculateGain($(this).val());
    $.getJSON( "/api/input/0/gain/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#input1 input.gain').on( "change mousemove", function() {
    $('#input1 span.gain').html(gainToString(calculateGain($(this).val())));
});
$('#input1 input.gain').on( "change", function() {
    channels.input[1].gain = calculateGain($(this).val());
    $.getJSON( "/api/input/1/gain/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#input2 input.gain').on( "change mousemove", function() {
    $('#input2 span.gain').html(gainToString(calculateGain($(this).val())));
});
$('#input2 input.gain').on( "change", function() {
    channels.input[2].gain = calculateGain($(this).val());
    $.getJSON( "/api/input/2/gain/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output0 input.gain').on( "change mousemove", function() {
    $('#output0 span.gain').html(gainToString(calculateGain($(this).val())));
});
$('#output0 input.gain').on( "change", function() {
    channels.output[0].gain = calculateGain($(this).val());
    $.getJSON( "/api/output/0/gain/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output1 input.gain').on( "change mousemove", function() {
    $('#output1 span.gain').html(gainToString(calculateGain($(this).val())));
});
$('#output1 input.gain').on( "change", function() {
    channels.output[1].gain = calculateGain($(this).val());
    $.getJSON( "/api/output/1/gain/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output2 input.gain').on( "change mousemove", function() {
    $('#output2 span.gain').html(gainToString(calculateGain($(this).val())));
});
$('#output2 input.gain').on( "change", function() {
    channels.output[2].gain = calculateGain($(this).val());
    $.getJSON( "/api/output/2/gain/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output3 input.gain').on( "change mousemove", function() {
    $('#output3 span.gain').html(gainToString(calculateGain($(this).val())));
});
$('#output3 input.gain').on( "change", function() {
    channels.output[3].gain = calculateGain($(this).val());
    $.getJSON( "/api/output/3/gain/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output4 input.gain').on( "change mousemove", function() {
    $('#output4 span.gain').html(gainToString(calculateGain($(this).val())));
});
$('#output4 input.gain').on( "change", function() {
    channels.output[4].gain = calculateGain($(this).val());
    $.getJSON( "/api/output/4/gain/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output5 input.gain').on( "change mousemove", function() {
    $('#output5 span.gain').html(gainToString(calculateGain($(this).val())));
});
$('#output5 input.gain').on( "change", function() {
    channels.output[5].gain = calculateGain($(this).val());
    $.getJSON( "/api/output/5/gain/"+$(this).val(), function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#input0 .mute').on( "click", function() {
    channels.input[0].mute = (channels.input[0].mute==1)?0:1;
    $.getJSON( "/api/input/0/mute/"+channels.input[0].mute, function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
        else
        {
            setInputMute( channels.input[0].mute, 0 );
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#input1 .mute').on( "click", function() {
    channels.input[1].mute = (channels.input[1].mute==1)?0:1;
    $.getJSON( "/api/input/1/mute/"+channels.input[1].mute, function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
        else
        {
            setInputMute( channels.input[1].mute, 1 )
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#input2 .mute').on( "click", function() {
    channels.input[2].mute = (channels.input[2].mute==1)?0:1;
    $.getJSON( "/api/input/2/mute/"+channels.input[2].mute, function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
        else
        {
            setInputMute( channels.input[2].mute, 2 )
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output0 .mute').on( "click", function() {
    channels.output[0].mute = (channels.output[0].mute==1)?0:1;
    $.getJSON( "/api/output/0/mute/"+channels.output[0].mute, function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
        else
        {
            setOutputMute( channels.output[0].mute, 0 );
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output1 .mute').on( "click", function() {
    channels.output[1].mute = (channels.output[1].mute==1)?0:1;
    $.getJSON( "/api/output/1/mute/"+channels.output[1].mute, function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
        else
        {
            setOutputMute( channels.output[1].mute, 1 );
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output2 .mute').on( "click", function() {
    channels.output[2].mute = (channels.output[2].mute==1)?0:1;
    $.getJSON( "/api/output/2/mute/"+channels.output[2].mute, function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
        else
        {
            setOutputMute( channels.output[2].mute, 2 );
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output3 .mute').on( "click", function() {
    channels.output[3].mute = (channels.output[3].mute==1)?0:1;
    $.getJSON( "/api/output/3/mute/"+channels.output[3].mute, function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
        else
        {
            setOutputMute( channels.output[3].mute, 3 );
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output4 .mute').on( "click", function() {
    channels.output[4].mute = (channels.output[4].mute==1)?0:1;
    $.getJSON( "/api/output/4/mute/"+channels.output[4].mute, function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
        else
        {
            setOutputMute( channels.output[4].mute, 4 );
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

$('#output5 .mute').on( "click", function() {
    channels.output[5].mute = (channels.output[5].mute==1)?0:1;
    $.getJSON( "/api/output/5/mute/"+channels.output[5].mute, function( data ) {
        if( data.error == "NOCONNECT" )
        {
            console.log( "there was an error" );
            connected(false);
            $('#connectError').modal();
        }
        else
        {
            setOutputMute( channels.output[5].mute, 5 );
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        connected(false);
        $('#connectError').modal();
    })
});

function update( callback )
{
    $.getJSON( "/api", function( data ) {
        if( data.error == "NOCONNECT" )
        {
            $('#connectError').modal();
            callback(false);
        }
        else
        {
            channels = data.data;
            map = data.map;
            name = data.name;
            updateUI(channels);
            callback(true);
        }
    })
    .fail(function() {
        console.log( "Could not connect" );
        $('#connectError').modal();
        callback(false);
    })
}

function updateUI(data)
{    
    for( var i = 0; i < 3; i++ )
    {
        $('#input'+i+' .gain').val(data.input[i].gain);
        $('#input'+i+' span.gain').html(gainToString(calculateGain(data.input[i].gain)));
        setInputMute( data.input[i].mute, i );
    }
    
    for( var i = 0; i < 6; i++ )
    {
        $('#output'+i+' .gain').val(data.output[i].gain);
        $('#output'+i+' span.gain').html(gainToString(calculateGain(data.output[i].gain)));
        setOutputMute( data.output[i].mute, i );
        setFrequency(data.output[i].lowpass.frequency, i, 'lowpass');
        setFrequency(data.output[i].highpass.frequency, i, 'highpass');
        setType(data.output[i].lowpass.type, i, 'lowpass');
        setType(data.output[i].highpass.type, i, 'highpass');
    }
}

function setInputMute( mute, channel )
{
    if( mute == 1 )
    {
        $('#input'+channel+' .mute').removeClass('btn-success');
        $('#input'+channel+' .mute').addClass('btn-danger');
    }
    else
    {
        $('#input'+channel+' .mute').removeClass('btn-danger');
        $('#input'+channel+' .mute').addClass('btn-success');
    }
}

function setOutputMute( mute, channel )
{
    if( mute == 1 )
    {
        $('#output'+channel+' .mute').removeClass('btn-success');
        $('#output'+channel+' .mute').addClass('btn-danger');
    }
    else
    {
        $('#output'+channel+' .mute').removeClass('btn-danger');
        $('#output'+channel+' .mute').addClass('btn-success');
    }
}

function setFrequency( frequency, channel, pass )
{
    $('#output'+channel+' .'+pass+' input.frequency').val(frequency);
    $('#output'+channel+' .'+pass+' label.frequency').html(frequencyToString(map.frequency[frequency]));
}

function setType( type, channel, pass )
{
    $('#output'+channel+' .'+pass+' select.type').val(type);
}

function connected(con)
{
    if(con)
    {
        $( "body" ).removeClass("disconnected");
    }
    else
    {
        $( "body" ).attr( "class", "disconnected" );
    }
}

function frequencyToString(frequency)
{
    return frequency+"&nbsp;hz";
}

function gainToString( gain )
{
    return ((gain>=0)?"+"+gain:gain)+"&nbsp;db";
}

function calculateValueFromGain(gain)
{
    return (gain*10)+150;
}

function calculateGain(value)
{
    return (value-150)/10;
}